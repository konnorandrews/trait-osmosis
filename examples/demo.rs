use core::fmt::{self, Debug, Error, Formatter};
use core::marker::PhantomData;

use trait_types::*;

/// Function with dynamic trait bound.
fn demo<T, Bound: Bind<T>>(x: &T)
where
    <Bound as Bind<T>>::Info: debug::Info<T = T>, // We require that the bound knows about Debug.
{
    if <<<Bound as Bind<T>>::Info as debug::Info>::Info as IsRequired>::Required::VALUE {
        println!("{:?}", Wrapper::<T, Bound>(x, PhantomData));
    } else {
        println!("maybe debug");
    }
}

fn demo2<T, Bound: Bind<T>>(x: &T) {}

struct X;

fn main() {
    demo::<i32, debug::Bound<True>>(&43);
    demo::<i32, debug::Bound<False>>(&43);
}

type_trait! {
    /// Type encoding for [`std::fmt::Display`].
    pub impl debug (std::fmt::Debug) {
        /// A
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
            this.fmt(f)
        }

        /// B
        fn to_string | to_string_unchecked(this: &Self::T) -> String {
            format!("{:?}", this)
        }
    }
}

struct Wrapper<'a, T, Bound>(&'a T, PhantomData<Bound>);

impl<'a, T, Bound: Bind<T>> fmt::Debug for Wrapper<'a, T, Bound>
where
    <Bound as Bind<T>>::Info: debug::Info<T = T>,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if <<<Bound as Bind<T>>::Info as debug::Info>::Info as IsRequired>::Required::VALUE {
            write!(f, "is debug: ")?;
            unsafe {
                <<<Bound as Bind<T>>::Info as debug::Info>::Info as debug::VTable>::fmt_unchecked(self.0, f)
            }
        } else {
            write!(f, "not debug")
        }
    }
}
