// #![no_std]

mod r#macro;
pub mod traits;

pub trait Bind<T: ?Sized> {
    type Info: ForType<T = T>;
}

pub trait VTableFor<Bound> {
    const INSTANCE: Self;
}

pub type InfoFor<T, Bound> = <Bound as Bind<T>>::Info;

pub trait ForType {
    type T: ?Sized;
}

pub trait IsRequired {
    type Required: Bool;
}

pub enum ConstBoolToType<const BOOL: bool> {}

impl IsRequired for ConstBoolToType<true> {
    type Required = True;
}

impl IsRequired for ConstBoolToType<false> {
    type Required = False;
}

pub enum True {}
pub enum False {}

pub trait Bool: sealed::Sealed {
    const VALUE: bool;
}

impl Bool for True {
    const VALUE: bool = true;
}

impl Bool for False {
    const VALUE: bool = false;
}

mod sealed {
    pub trait Sealed {}

    impl Sealed for super::True {}
    impl Sealed for super::False {}
}


// type_trait! {
//     /// Type encoding for [`std::fmt::Display`].
//     pub impl display (std::fmt::Display) {
//         /// A
//         fn fmt | fmt_unchecked(this: &Self::T, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
//             this.fmt(f)
//         }
//
//         /// B
//         fn to_string | to_string_unchecked(this: &Self::T) -> String {
//             format!("{}", this)
//         }
//     }
// }
//
// type_trait! {
//     /// Type encoding for [`Clone`].
//     pub impl clone (Clone) {
//         /// Forwards to [`Self::T`][ForType::T]'s impl of [`Clone::clone`].
//         fn clone | clone_unchecked(this: &Self::T) -> Self::T where { Self::T: Sized } {
//             this.clone()
//         }
//     }
// }

// type_trait! {
//     pub impl<U: (?Sized)> as_ref (AsRef<U>) {
//         fn as_ref | as_ref_unchecked(this: &Self::T) -> &U {
//             this.as_ref()
//         }
//     }
// }
//
// type_trait! {
//     pub impl<Output> future (std::future::Future<Output = Output>) {
//         fn poll | poll_unchecked(this: std::pin::Pin<&mut Self::T>, cx: &mut std::task::Context<'_>) 
//             -> std::task::Poll<Output>
//         {
//             todo!()
//         }
//     }
// }
//
// type_trait! {
//     pub impl send (Send) {}
// }

use traits::{clone, display};

maybe_bind_vtable! {
    pub struct CloneVTable<T> (clone) {
        clone | clone_unchecked: unsafe fn(this: &T) -> T,
    }
}

maybe_bind_vtable! {
    pub struct DisplayVTable<T: (?Sized)> (display) {
        fmt | fmt_unchecked: unsafe fn(this: &T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error>,
    }
}

// fn demo<T, Bound: Bind<T>>(x: &T)
// where
//     InfoFor<T, Bound>: display::Info,
// {
//     if display::RequiredFor::<InfoFor<T, Bound>>::VALUE {
//         println!("is display: {}", unsafe {
//             <display::InfoFor<InfoFor<T, Bound>> as display::VTable>::to_string_unchecked(x)
//         });
//     } else {
//         println!("not display")
//     }
// }

// fn demo3<T, Bound: Bind<T>>(x: &T) -> T
// where
//     InfoFor<T, Bound>: clone::Info,
// {
//     if clone::RequiredFor::<InfoFor<T, Bound>>::VALUE {
//         println!("is clone");
//         unsafe { <clone::InfoFor<InfoFor<T, Bound>> as clone::VTable>::clone_unchecked(x) }
//     } else {
//         println!("not clone");
//         todo!()
//     }
// }

// fn demo4<T, Bound: Bind<T>, Output>(x: &T) -> Output
// where
//     InfoFor<T, Bound>: future::Info<Output>,
// {
//     if future::RequiredFor::<InfoFor<T, Bound>, Output>::VALUE {
//         println!("is future");
//     } else {
//         println!("not future");
//     }
//     todo!()
// }

// fn test<T>(vtable: CloneVTable<T>, x: &T) -> T {
//     vtable.clone(x)
// }
//

use std::fmt;

/// A bound `B` is of the form `a + b + c`.
/// When we bind `T` by `B` we gain access to the methods on `a`, `b`, and `c`.
///
/// A membrane `M` is of the form `Maybe<a> + Maybe<b> + Maybe<c>`
/// where each member trait is either required or not.
/// Each configuration of a membrane gives a concrete bound.
///
/// Every membrane `M` has a set of traits `a`, `b`, `c` that it cares about.
/// traits outside of this set are not considered.
/// Interesting properties for a membrane to express: trait set
struct _Demo;

mod demo {
    trait Bound {
        type TraitSet;
    }

    trait Membrane {}
}

struct Display<'a, T: ?Sized>(&'a T, DisplayVTable<T>);

impl<'a, T: ?Sized> fmt::Display for Display<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.1.is_required() {
            self.1.fmt(&self.0, f)
        } else {
            write!(f, "<unknown>")
        }
    }
}


#[test]
fn demo2() {
    let v = vtable!(i32, DisplayVTable, display);
    println!("thing: {}", Display(&42, v));

    let v = vtable!([i32], DisplayVTable, display);
    println!("thing: {}", Display(&vec![42] as &_, v));

    todo!();
}

