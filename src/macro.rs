#[macro_export]
macro_rules! type_trait {
    {
        $(#[$($meta:tt)*])*
        $vis:vis impl$(<$($main_generic:ident$(: ($($generic_bound:tt)*))?),*>)? $name:ident ($($bound:tt)*) {$(
            $(#[$($method_meta:tt)*])*
            fn $method:ident | $method_unchecked:ident $([$($generic:tt)*])?($($arg:ident: $arg_ty:ty),*) $(-> $return:ty)?
            $(where {$($where:tt)*})? {
                $($code:tt)*
            }
        )*}
    } => {
        #[doc = concat!("Type encoding for trait bound `T: ", stringify!($($bound)*) , "`")]
        $(#[$($meta)*])*
        $vis mod $name {
            use ::core::marker::PhantomData;
            use $crate::*;

            #[doc = concat!("Marker type for trait bound `T: ", stringify!($($bound)*) , "`")]
            ///
            /// When `Required` is [`True`] then the bound is required.
            ///
            /// When `Required` is [`False`] then the bound is not required.
            #[doc = concat!("This is the same as not bounding on `", stringify!($($bound)*), "` at all.")]
            pub struct Bound<Required $($(, $main_generic$(: $($generic_bound)*)?)*)?>(PhantomData<(Required, $($(fn() -> *const $main_generic),*)?)>);

            #[doc = concat!("Trait for bounds that have the info for a `", stringify!($($bound)*), "` bound.")]
            pub trait Info$(<$($main_generic$(: $($generic_bound)*)?),*>)?: ForType {
                #[doc = concat!("Info about the dynamic bound made on `", stringify!($($bound)*), "`.")]
                ///
                /// This includes if the bound was actually required (via [`IsRequired`]).
                /// Additionally, it includes a vtable to call methods the bound my provide.
                type Info: VTable$(<$($main_generic),*>)? + IsRequired + ForType<T = Self::T>;
            }

            #[doc = concat!("Virtual method table for a type bound by `", stringify!($($bound)*), "`.")]
            pub trait VTable$(<$($main_generic$(: $($generic_bound)*)?),*>)?: IsRequired + ForType + sealed::Sealed {$(
                $(#[$($method_meta)*])*
                ///
                /// # Safety
                /// This function must only be called if `<Self as IsRequired>::Required = True`.
                /// This can be checked at compile time via the `Self: IsRequired<Required = True>` trait bound.
                #[doc = concat!("See [`Self::", stringify!($method), "`] for an example of that.")]
                /// The condition can also be checked at runtime via `<Self as
                /// IsRequired>::Required::Value == true`.
                unsafe fn $method_unchecked$(<$($generic)*>)?($($arg: $arg_ty),*) $(-> $return)?
                $(where $($where)*)?;

                #[doc = concat!("Safe wrapper around [`Self::", stringify!($method_unchecked) ,"`].")]
                ///
                /// Safety is provided by the bound on [`IsRequired<Required = True>`][IsRequired].
                fn $method$(<$($generic)*>)?($($arg: $arg_ty),*) $(-> $return)?
                where
                    Self: IsRequired<Required = True>,
                    $($($where)*)?
                {
                    unsafe { Self::$method_unchecked($($arg),*) }
                }
            )*}

            mod sealed {
                use super::*;

                pub trait Sealed {}

                pub struct InfoImpl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?>(PhantomData<(fn() -> *const T, Required $($(, fn() -> *const $main_generic)*)?)>);

                impl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?> Sealed for InfoImpl<T, Required $($(, $main_generic)*)?> {}

                impl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?> Bind<T> for Bound<Required $($(, $main_generic)*)?>
                where
                    InfoImpl<T, Required $($(, $main_generic)*)?>: Info<$($($main_generic),*,)? T = T>,
                {
                    type Info = InfoImpl<T, Required $($(, $main_generic)*)?>;
                }

                impl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?> ForType for InfoImpl<T, Required $($(, $main_generic)*)?> {
                    type T = T;
                }

                impl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?> IsRequired for InfoImpl<T, Required $($(, $main_generic)*)?> {
                    type Required = Required;
                }

                impl<T: ?Sized, Required: Bool $($(, $main_generic$(: $($generic_bound)*)?)*)?> Info$(<$($main_generic),*>)? for InfoImpl<T, Required $($(, $main_generic)*)?> where Self: VTable$(<$($main_generic),*>)? {
                    type Info = Self;
                }

                impl<T: ?Sized + $($bound)* $($(, $main_generic$(: $($generic_bound)*)?)*)?> VTable$(<$($main_generic),*>)? for InfoImpl<T, True $($(, $main_generic)*)?> {$(
                    unsafe fn $method_unchecked$(<$($generic)*>)?($($arg: $arg_ty),*) $(-> $return)?
                    $(where $($where)*)? {
                        $($code)*
                    }
                )*}

                #[allow(unused)]
                impl<T: ?Sized $($(, $main_generic$(: $($generic_bound)*)?)*)?> VTable$(<$($main_generic),*>)? for InfoImpl<T, False $($(, $main_generic)*)?> {$(
                    unsafe fn $method_unchecked$(<$($generic)*>)?($($arg: $arg_ty),*) $(-> $return)?
                    $(where $($where)*)? {
                        unsafe { ::core::hint::unreachable_unchecked() }
                    }
                )*}
            }

            pub type InfoFor<T$(, $($main_generic),*)?> = <T as Info$(<$($main_generic),*>)?>::Info;
            pub type RequiredFor<T$(, $($main_generic),*)?> = <<T as Info$(<$($main_generic),*>)?>::Info as IsRequired>::Required;
        }
    }
}

#[macro_export]
macro_rules! impls_bound {
    ($type:ty, $($bound:tt)*) => {
        <ConstBoolToType<{
            trait DoesNotImpl {
                const IMPLS: bool = false;
            }
            impl<T: ?Sized> DoesNotImpl for T {}

            struct Wrapper<T: ?Sized>(core::marker::PhantomData<T>);

            #[allow(dead_code)]
            impl<T: ?Sized> Wrapper<T> where $($bound)*::Bound<True>: Bind<T> {
                const IMPLS: bool = true;
            }

            Wrapper::<$type>::IMPLS
        }> as IsRequired>::Required
    }
}

#[macro_export]
macro_rules! maybe_bind_vtable {
    {
        $vis:vis struct $name:ident<$t:ident$(: ($($t_bound:tt)*))?> ($bound:ident) {
            $(
                $method:ident | $method_unchecked:ident: unsafe fn ($($arg:ident: $arg_ty:ty),*) $(-> $return:ty)?,
            )*
        }
    } => {
        #[derive(Clone, Copy)]
        $vis struct $name<$t$(: $($t_bound)*)?> {
            $(
                $method_unchecked: unsafe fn ($($arg_ty),*) $(-> $return)?,
            )*
            required: bool,
            bound_name: fn() -> &'static str,
        }

        impl<$t$(: $($t_bound)*)?> $name<$t> {
            $(
                pub unsafe fn $method_unchecked(&self, $($arg: $arg_ty),*) $(-> $return)? {
                    ::core::debug_assert!(self.required);
                    (self.$method_unchecked)($($arg),*)
                }

                pub fn $method(&self, $($arg: $arg_ty),*) $(-> $return)? {
                    if self.required {
                        unsafe { (self.$method_unchecked)($($arg),*) }
                    } else {
                        panic!(concat!("called `", stringify!($method), "` when the bound `{}` didn't require it to be implemented"), (self.bound_name)())
                    }
                }
            )*

            pub fn is_required(&self) -> bool {
                self.required
            }
        }

        impl<$t$(: $($t_bound)*)?, Bound: $crate::Bind<$t>> $crate::VTableFor<Bound> for $name<$t> 
        where
            <Bound as $crate::Bind<$t>>::Info: $bound::Info,
        {
            const INSTANCE: $name<$t> = $name { 
                $(
                    $method_unchecked: <<<Bound as $crate::Bind<$t>>::Info as $bound::Info>::Info as $bound::VTable>::$method_unchecked,
                )*
                required: <<<Bound as $crate::Bind<T>>::Info as $bound::Info>::Info as $crate::IsRequired>::Required::VALUE,
                bound_name: ::core::any::type_name::<Bound>
            };
        }
    }
}

#[macro_export]
macro_rules! vtable {
    ($type:ty, $table:ident, $($bound:tt)*) => {
        <$table<$type> as $crate::VTableFor<$($bound)*::Bound<impls_bound!($type, $($bound)*)>>>::INSTANCE
    }
}
