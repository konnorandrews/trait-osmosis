use crate::type_trait;

type_trait! {
    pub impl any (core::any::Any) {
        fn type_id | type_id_unchecked(this: &Self::T) -> core::any::TypeId {
            this.type_id()
        }
    }
}

type_trait! {
    pub impl<Borrowed> borrowed (core::borrow::Borrow<Borrowed>) {
        fn borrow | borrow_unchecked(this: &Self::T) -> &Borrowed {
            this.borrow()
        }
    }
}

type_trait! {
    pub impl<Borrowed> borrowed_mut (core::borrow::BorrowMut<Borrowed>) {
        fn borrow | borrow_unchecked(this: &Self::T) -> &Borrowed {
            this.borrow()
        }

        fn borrow_mut | borrow_mut_unchecked(this: &mut Self::T) -> &mut Borrowed {
            this.borrow_mut()
        }
    }
}

type_trait! {
    pub impl clone (core::clone::Clone) {
        fn clone | clone_unchecked(this: &Self::T) -> Self::T where { Self::T: Sized } {
            this.clone()
        }

        fn clone_from | clone_from_unchecked(this: &mut Self::T, source: &Self::T) {
            this.clone_from(source)
        }
    }
}

type_trait! {
    pub impl<Rhs: (?Sized)> partial_eq (core::cmp::PartialEq<Rhs>) {
        fn eq | eq_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.eq(other)
        }

        fn ne | ne_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.ne(other)
        }
    }
}

type_trait! {
    pub impl eq (core::cmp::Eq) {
        fn eq | eq_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.eq(other)
        }

        fn ne | ne_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.ne(other)
        }
    }
}

type_trait! {
    pub impl<Rhs: (?Sized)> partial_ord (core::cmp::PartialOrd<Rhs>) {
        fn eq | eq_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.eq(other)
        }

        fn ne | ne_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.ne(other)
        }

        fn partial_cmp | partial_cmp_unchecked(this: &Self::T, other: &Rhs) -> Option<core::cmp::Ordering> {
            this.partial_cmp(other)
        }

        fn lt | lt_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.lt(other)
        }

        fn le | le_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.le(other)
        }

        fn gt | gt_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.gt(other)
        }

        fn ge | ge_unchecked(this: &Self::T, other: &Rhs) -> bool {
            this.ge(other)
        }
    }
}

type_trait! {
    pub impl ord (core::cmp::Ord) {
        fn eq | eq_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.eq(other)
        }

        fn ne | ne_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.ne(other)
        }

        fn partial_cmp | partial_cmp_unchecked(this: &Self::T, other: &Self::T) -> Option<core::cmp::Ordering> {
            this.partial_cmp(other)
        }

        fn lt | lt_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.lt(other)
        }

        fn le | le_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.le(other)
        }

        fn gt | gt_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.gt(other)
        }

        fn ge | ge_unchecked(this: &Self::T, other: &Self::T) -> bool {
            this.ge(other)
        }

        fn cmp | cmp_unchecked(this: &Self::T, other: &Self::T) -> core::cmp::Ordering {
            this.cmp(other)
        }

        fn max | max_unchecked(this: Self::T, other: Self::T) -> Self::T where { Self::T: Sized } {
            this.max(other)
        }

        fn min | min_unchecked(this: Self::T, other: Self::T) -> Self::T where { Self::T: Sized } {
            this.min(other)
        }

        fn clamp | clamp_unchecked(this: Self::T, min: Self::T, max: Self::T) -> Self::T where { Self::T: Sized } {
            this.clamp(min, max)
        }
    }
}

type_trait! {
    pub impl<U: (?Sized)> as_ref (core::convert::AsRef<U>) {
        fn as_ref | as_ref_unchecked(this: &Self::T) -> &U {
            this.as_ref()
        }
    }
}

type_trait! {
    pub impl<U: (?Sized)> as_mut (core::convert::AsMut<U>) {
        fn as_mut | as_mut_unchecked(this: &mut Self::T) -> &mut U {
            this.as_mut()
        }
    }
}

type_trait! {
    pub impl<U> from (core::convert::From<U>) {
        fn from | from_unchecked(value: U) -> Self::T where { Self::T: Sized } {
            Self::T::from(value)
        }
    }
}

type_trait! {
    pub impl<U> into (core::convert::Into<U>) {
        fn into | into_unchecked(this: Self::T) -> U where { Self::T: Sized } {
            this.into()
        }
    }
}

type_trait! {
    pub impl<U, Error> try_from (core::convert::TryFrom<U, Error = Error>) {
        fn try_from | try_from_unchecked(value: U) -> Result<Self::T, Error> where { Self::T: Sized } {
            Self::T::try_from(value)
        }
    }
}

type_trait! {
    pub impl<U, Error> try_into (core::convert::TryInto<U, Error = Error>) {
        fn try_into | try_into_unchecked(this: Self::T) -> Result<U, Error> where { Self::T: Sized } {
            this.try_into()
        }
    }
}

type_trait! {
    pub impl default (core::default::Default) {
        fn default | default_unchecked() -> Self::T where { Self::T: Sized } {
            Self::T::default()
        }
    }
}

type_trait! {
    pub impl binary (core::fmt::Binary) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl debug (core::fmt::Debug) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl display (core::fmt::Display) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl lower_exp (core::fmt::LowerExp) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl lower_hex (core::fmt::LowerHex) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl octal (core::fmt::Octal) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl pointer (core::fmt::Pointer) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl upper_exp (core::fmt::UpperExp) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl upper_hex (core::fmt::UpperHex) {
        fn fmt | fmt_unchecked(this: &Self::T, f: &mut core::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
            this.fmt(f)
        }
    }
}

type_trait! {
    pub impl fmt_write (core::fmt::Write) {
        fn write_str | write_str_unchecked(this: &mut Self::T, s: &str) -> Result<(), core::fmt::Error> {
            this.write_str(s)
        }

        fn write_char | write_char_unchecked(this: &mut Self::T, c: char) -> Result<(), core::fmt::Error> {
            this.write_char(c)
        }

        fn write_fmt | write_fmt_unchecked(this: &mut Self::T, args: core::fmt::Arguments<'_>) -> Result<(), core::fmt::Error> {
            this.write_fmt(args)
        }
    }
}

type_trait! {
    pub impl<Output> future (core::future::Future<Output = Output>) {
        fn poll | poll_unchecked(this: core::pin::Pin<&mut Self::T>, cx: &mut core::task::Context<'_>) -> core::task::Poll<Output> {
            this.poll(cx)
        }
    }
}

type_trait! {
    pub impl<Output, IntoFuture> into_future (core::future::IntoFuture<Output = Output, IntoFuture = IntoFuture>) {
        fn into_future | into_future_unchecked(this: Self::T) -> IntoFuture where { Self::T: Sized } {
            this.into_future()
        }
    }
}

type_trait! {
    pub impl<Hasher> build_hasher (core::hash::BuildHasher<Hasher = Hasher>) {
        fn build_hasher | build_hasher_unchecked(this: &Self::T) -> Hasher {
            this.build_hasher()
        }

        fn hash_one | hash_one_unchecked[U: core::hash::Hash](this: &Self::T, x: U) -> u64
        where {
            Self::T: Sized,
            Hasher: core::hash::Hasher
        } {
            this.hash_one(x)
        }
    }
}

type_trait! {
    pub impl hash (core::hash::Hash) {
        fn hash | hash_unchecked[H: core::hash::Hasher](this: &Self::T, state: &mut H) {
            this.hash(state)
        }

        fn hash_slice | hash_slice_unchecked[H: core::hash::Hasher](data: &[Self::T], state: &mut H)
        where {
            Self::T: Sized,
        } {
            Self::T::hash_slice(data, state)
        }
    }
}

type_trait! {
    pub impl hasher (core::hash::Hasher) {
        fn finish | finish_unchecked(this: &Self::T) -> u64 {
            this.finish()
        }

        fn write | write_unchecked(this: &mut Self::T, bytes: &[u8]) {
            this.write(bytes)
        }

        fn write_u8 | write_u8_unchecked(this: &mut Self::T, i: u8) {
            this.write_u8(i)
        }

        fn write_u16 | write_u16_unchecked(this: &mut Self::T, i: u16) {
            this.write_u16(i)
        }

        fn write_u32 | write_u32_unchecked(this: &mut Self::T, i: u32) {
            this.write_u32(i)
        }

        fn write_u64 | write_u64_unchecked(this: &mut Self::T, i: u64) {
            this.write_u64(i)
        }

        fn write_u128 | write_u128_unchecked(this: &mut Self::T, i: u128) {
            this.write_u128(i)
        }

        fn write_usize | write_usize_unchecked(this: &mut Self::T, i: usize) {
            this.write_usize(i)
        }

        fn write_i8 | write_i8_unchecked(this: &mut Self::T, i: i8) {
            this.write_i8(i)
        }

        fn write_i16 | write_i16_unchecked(this: &mut Self::T, i: i16) {
            this.write_i16(i)
        }

        fn write_i32 | write_i32_unchecked(this: &mut Self::T, i: i32) {
            this.write_i32(i)
        }

        fn write_i64 | write_i64_unchecked(this: &mut Self::T, i: i64) {
            this.write_i64(i)
        }

        fn write_i128 | write_i128_unchecked(this: &mut Self::T, i: i128) {
            this.write_i128(i)
        }

        fn write_isize | write_isize_unchecked(this: &mut Self::T, i: isize) {
            this.write_isize(i)
        }
    }
}

type_trait! {
    pub impl<Item> iterator (core::iter::Iterator<Item = Item>) {
        fn next | next_unchecked(this: &mut Self::T) -> Option<Item> {
            this.next()
        }
    }
}

type_trait! {
    pub impl<Item> double_ended_iterator (core::iter::DoubleEndedIterator<Item = Item>) {
        fn next | next_unchecked(this: &mut Self::T) -> Option<Item> {
            this.next()
        }

        fn next_back | next_back_unchecked(this: &mut Self::T) -> Option<Item> {
            this.next_back()
        }
    }
}

type_trait! {
    pub impl<Item> exact_size_iterator (core::iter::ExactSizeIterator<Item = Item>) {
        fn next | next_unchecked(this: &mut Self::T) -> Option<Item> {
            this.next()
        }

        fn len | len_unchecked(this: &Self::T) -> usize {
            this.len()
        }
    }
}

type_trait! {
    pub impl<A> extend (core::iter::Extend<A>) {
        fn extend | extend_unchecked[I: IntoIterator<Item = A>](this: &mut Self::T, iter: I) {
            this.extend(iter)
        }
    }
}

type_trait! {
    pub impl<A> from_iterator (core::iter::FromIterator<A>) {
        fn from_iter | from_iter_unchecked[I: IntoIterator<Item = A>](iter: I) -> Self::T where { Self::T: Sized } {
            Self::T::from_iter(iter)
        }
    }
}

type_trait! {
    pub impl<Item> fused_iterator (core::iter::FusedIterator<Item = Item>) {
        fn next | next_unchecked(this: &mut Self::T) -> Option<Item> {
            this.next()
        }
    }
}

type_trait! {
    pub impl<Item, IntoIter> into_iterator (core::iter::IntoIterator<Item = Item, IntoIter = IntoIter>) {
        fn into_iter | into_iter_unchecked(this: Self::T) -> IntoIter where { Self::T: Sized } {
            this.into_iter()
        }
    }
}

type_trait! {
    pub impl<A> product (core::iter::Product<A>) {
        fn product | product_unchecked[I: core::iter::Iterator<Item = A>](iter: I) -> Self::T where { Self::T: Sized } {
            Self::T::product(iter)
        }
    }
}

type_trait! {
    pub impl<A> sum (core::iter::Sum<A>) {
        fn sum | sum_unchecked[I: core::iter::Iterator<Item = A>](iter: I) -> Self::T where { Self::T: Sized } {
            Self::T::sum(iter)
        }
    }
}

type_trait! {
    pub impl copy (core::marker::Copy) {
        fn clone | clone_unchecked(this: &Self::T) -> Self::T where { Self::T: Sized } {
            this.clone()
        }

        fn clone_from | clone_from_unchecked(this: &mut Self::T, source: &Self::T) {
            this.clone_from(source)
        }

        fn copy | copy_unchecked(this: &Self::T) -> Self::T where { Self::T: Sized } {
            *this
        }
    }
}

type_trait! {
    pub impl send (core::marker::Send) {}
}

type_trait! {
    pub impl sync (core::marker::Sync) {}
}

type_trait! {
    pub impl sized (core::marker::Sized) {
        fn size_of | size_of_unchecked() -> usize {
            core::mem::size_of::<Self::T>()
        }

        fn align_of | align_of_unchecked() -> usize {
            core::mem::align_of::<Self::T>()
        }
    }
}

type_trait! {
    pub impl unpin (core::marker::Unpin) {}
}

type_trait! {
    pub impl<Rhs, Output> add (core::ops::Add<Rhs, Output = Output>) {
        fn add | add_unchecked(this: Self::T, rhs: Rhs) -> Output where { Self::T: Sized } {
            this.add(rhs)
        }
    }
}

type_trait! {
    pub impl<Rhs> add_assign (core::ops::AddAssign<Rhs>) {
        fn add_assign | add_assign_unchecked(this: &mut Self::T, rhs: Rhs) {
            this.add_assign(rhs)
        }
    }
}

type_trait! {
    pub impl<Rhs, Output> bit_and (core::ops::BitAnd<Rhs, Output = Output>) {
        fn bitand | bitand_unchecked(this: Self::T, rhs: Rhs) -> Output where { Self::T: Sized } {
            this.bitand(rhs)
        }
    }
}

type_trait! {
    pub impl<Rhs> bit_and_assign (core::ops::BitAndAssign<Rhs>) {
        fn bitand_assign | bitand_assign_unchecked(this: &mut Self::T, rhs: Rhs) {
            this.bitand_assign(rhs)
        }
    }
}
